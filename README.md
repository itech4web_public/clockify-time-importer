# Description
Simple applications that allow to import the clockify time from csv format 
without having access to project settings.
You only need generate the API key in you clockify profile.

### Generate api key in clockify
Go to you [profile](https://clockify.me/user/settings) settings and generate the key.
[Here](https://clockify.me/help/time-tracking/jira-integration#api-key) is also instruction on how to generate the api key from clockify. 

### Setup api key
1. Create file `config/services.local.yaml` from `config/services.local.yaml.dist`
2. Put the API key to `services.local.yaml`

### CSV format

CSV example:
```
Day,Time Entry,Project,Task,Time
YYY-MM-DD,Time log desription,Project1,Task1,HH:MM:SS
```
Human representation with example:

|Day       |Time Entry         |Project |Task |Time   |
|----------|-------------------|--------|-----|-------|
|2022-04-01|Time log desription|Project1|Task1|0:30:00|


## Using the app
Before start make sure you have [symfony cli](https://symfony.com/download) installed.   

1. Start the server to begin using the app from the project root, type in CLI:
`symfony server:start`
2. Visit the url provided by the symfony server;
3. Select the destination Workspace;
4. Upload the csv file in `CSV` field and click `Upload & Validate`;
5. If no errors - select the desired workspace again in bottom `Workspace` field;
6. Click `Import`.

### Other useful server commands:
`symfony server:start -d` 
`symfony server:log`
`symfony server:stop`

## For developers

### Build the scss:

`yarn encore dev`

### node version:

`10.13` (`$ nvm use 10.13`)

### Generate the classes from json schema:

`./vendor/bin/s2c generate:fromschema --class ClockifyTimeEntry ./timeEntryJSONSchema.yaml src/Repository`

### Clear cache when developing:

`php bin/console cache:pool:clear cache.global_clearer`

### Clear prod cache:

`APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear`  
