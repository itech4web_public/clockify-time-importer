<?php

namespace App\Controller;

use App\Form\ClockifyFileUploadType;
use App\Form\ClockifyTimeExporterType;
use App\Repository\ClockifyTimeEntry;
use League\Csv\HTMLConverter;
use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DefaultController extends AbstractController
{

  private $apiClient;

  public function __construct(HttpClientInterface $clockifyHttpClient) {
    $this->apiClient = $clockifyHttpClient;
  }

    /**
   * The path to currently active file.
   *
   * @var string
   */
  const CSV_FILE_PATH = 'uploads/export.csv';

  /**
   * Index page callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function index(Request $request): Response {

    $upload_form = $this->createForm(ClockifyFileUploadType::class, NULL, );
    $upload_form->handleRequest($request);
    $csv_is_validated = FALSE;
    $csv_errors = [];
    if ($upload_form->isSubmitted() && $upload_form->isValid()) {
        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
        $file = $upload_form['csv']->getData();
        $file->move('uploads', 'export.csv');

      /** @var \Symfony\Component\Form\Form $form_element */
      $csv_errors = $this->validateCsv($upload_form['workspaces']->getData());
      $csv_is_validated = TRUE;
    }

    $reader = $this->loadCsvFile();
    $table = NULL;
    $total_records = 0;

    if ($reader instanceof Reader) {
      $records = $reader->getRecords($reader->getHeader());
      $total_records = $reader->count();
      $classes = [
        'table',
        'table-striped',
        'table-bordered',
        'table-hover',
        'table-sm',
        'table-responsive'
      ];
      $htmlConverter = (new HTMLConverter())->table(implode(' ', $classes));
      $table = $htmlConverter->convert($records, $reader->getHeader());
    }
    else {
      $csv_errors = $reader;
    }

    $import_form = $this->createForm(ClockifyTimeExporterType::class, NULL, );
    $import_form->handleRequest($request);

    $import_errors = [];
    if ($import_form->isSubmitted() && $import_form->isValid()) {
      $import_errors = $this->importWorkEntities($import_form['workspaces']->getData());
    }

    return $this->render('form/clokify-import.html.twig', [
      'upload_form' => $upload_form->createView(),
      'import_form' => $import_form->createView(),
      'table' => $table,
      'total_records' => $total_records,
      'csv_errors' => $csv_errors,
      'import_errors' => $import_errors,
      'csv_is_validated' => $csv_is_validated,
      'total_time_to_import' => $reader instanceof Reader ? $this->getTotalTimeToImport($reader) : 0,
    ]);
  }

  /**
   * Calculate total time to import.
   *
   * @param \League\Csv\Reader $reader
   *
   * @return string
   */
  private function getTotalTimeToImport(Reader $reader): string {
    $seconds = 0;
    foreach($reader->getRecords($reader->getHeader()) as $rows) {
      $parsed_time = date_parse($rows['Time']);
      $seconds += $parsed_time['hour'] * 3600 + $parsed_time['minute'] * 60 + $parsed_time['second'];
    }

    return sprintf('%02dh %02dm', ($seconds/ 3600),($seconds/ 60 % 60));
  }

  /**
   * Import the work entities to clokify.
   *
   * @param string $workspaceId
   *
   * @return array
   * @throws \League\Csv\Exception
   */
  private function importWorkEntities(string $workspaceId): array {
    $reader = $this->loadCsvFile();

    if (!$reader instanceof Reader) {
      return $reader;
    }

    $message = [
      'total_imported' => 0,
      'total_rows_processed' => 0,
      'total_errors' => 0,
      'error' => '',
      'errors' => [],
    ];

    $reader->setHeaderOffset(0);
    $tz = new \DateTimeZone('Europe/Kiev');

    foreach ($reader->getRecords($reader->getHeader()) as $records) {
      $project = $this->findProjectByName($records['Project'], $workspaceId);

      if (isset($project['id'])) {
        $task = $this->findTaskByName($project['id'], $workspaceId, $records['Task']);
        if (isset($task['id'])) {

          $startDate = \DateTime::createFromFormat('Y-m-d H:i', $records['Day'] . ' 8:00', $tz);
          $endDate = clone $startDate;
          $endDate->add($this->convertDurationStringToInterval($records['Time']));

          $clockifyTimeEntry = ClockifyTimeEntry::buildFromInput(
            [
              'start' => $startDate->setTimezone(new \DateTimeZone('Z'))->format('c'),
              'end' => $endDate->setTimezone(new \DateTimeZone('Z'))->format('c'),
              'description' => $records['Time Entry'],
              'projectId' => $project['id'],
              'taskId' => $task['id'],
            ],
          );

          $result = $this->writeTimeEntryRecord($workspaceId, $clockifyTimeEntry);
          if (isset($result['id'])) {
            $message['total_imported']++;
          }
          else {
            $message['total_errors']++;
            $message['errors'][] = $result['error'];
          }

        }
        else {
          $message['total_errors']++;
          $message['errors'][] = $task['error'];
        }
      }
      else {
        $message['total_errors']++;
        $message['errors'][] = $project['error'];
      }

      $message['total_rows_processed']++;
    }

    return $message;

  }

  private function findProjectByName(string $projectName, string $workspaceId): array {

    try {
      $response = $this->apiClient->request('GET', "workspaces/{$workspaceId}/projects");
      $data = $response->toArray();
    }
    catch (\Exception $exception) {
      return ['error' => $exception->getMessage()];
    }
    foreach ($data as $data_item) {
      if ($data_item['name'] === $projectName) {
        return $data_item;
      }
    }

    return ['error' => 'Project not found'];
  }

  private function findTaskByName(string $projectId, string $workspaceId, string $taskName): array {

    try {
      $response = $this->apiClient->request('GET', "workspaces/{$workspaceId}/projects/{$projectId}/tasks");
      $data = $response->toArray();
    }
    catch (\Exception $exception) {
      return ['error' => $exception->getMessage()];
    }

    foreach ($data as $data_item) {
      if ($data_item['name'] === $taskName) {
        return $data_item;
      }
    }

    return ['error' => 'Task not found'];
  }

  /**
   * @return \League\Csv\Reader|array
   */
  private function loadCsvFile() {
    try {
      $file = new File(static::CSV_FILE_PATH);
      $file_path = $file->getRealPath();
      $reader = Reader::createFromPath($file_path, 'r');
      $reader->setHeaderOffset(0);
      $reader->getRecords($reader->getHeader());
      return $reader;
    }
    catch (\Exception $e) {
      return ['error' => $e->getMessage()];
    }
  }

  private function writeTimeEntryRecord(string $workspaceId, ClockifyTimeEntry $timeEntry): array {
    try {
      $options = [
        'json' => $timeEntry->toJson(),
      ];

      $options['json']['start'] = $timeEntry->getStart()->format('Y-m-d\TH:i:s\Z');
      $options['json']['end'] = $timeEntry->getEnd()->format('Y-m-d\TH:i:s\Z');

      $response = $this->apiClient->request('POST', "workspaces/{$workspaceId}/time-entries", $options);
      $data = $response->toArray();
    }
    catch (ClientException $exception) {
      return ['error' => json_decode($exception->getResponse()->getContent(FALSE), TRUE)['message']];
    }
    catch (\Exception $exception) {
      return ['error' => $exception->getMessage()];
    }

    return $data;
  }

  private function convertDurationStringToInterval(string $duration): \DateInterval {
    [$hours, $minutes, $seconds] = explode(':', $duration);
    return new \DateInterval("PT{$hours}H{$minutes}M{$seconds}S");
  }

  private function validateCsv(string $workspaceId): array {
    $requiredProperties = $this->getRequiredTableColumns();

    $workspaces = $this->getWorkspaces();
    $workspaceName = '';
    if ($workspaces) {
      foreach ($workspaces as $workspace) {
        $options[$workspace['id']] = $workspace['name'];
      }
      $workspaceName = $options[$workspaceId];
    }

    $reader = $this->loadCsvFile();

    $errors = [];
    if ($reader instanceof Reader) {
      foreach ($requiredProperties as $defaultProperty) {
        if (!in_array($defaultProperty, $reader->getHeader())) {
          $errors[] = "Column <em>$defaultProperty</em> is missing.";
        }
      }

      foreach ($reader->getRecords($reader->getHeader()) as $row) {
        $project = $this->findProjectByName($row['Project'], $workspaceId);
        if (empty($project['id'])) {
          $errors[] = "Project <em>{$row['Project']}</em> not found in <em>$workspaceName</em> workspace.";
        }
        else {
          $task = $this->findTaskByName($project['id'], $workspaceId,  $row['Task']);
          if (empty($task['id'])) {
            $errors[] = "Task <em>{$row['Task']}</em> not found in <em>$workspaceName</em> workspace for <em>{$row['Project']}</em> project.";
          }
        }
      }
    }
    else {
      $errors = $reader;
    }

    return $errors;
  }

  private function getWorkspaces(): array {

    try {
      $response = $this->apiClient->request('GET', 'workspaces');
      $workspaces_data = $response->toArray();
    }
    catch (\Exception $exception) {
      return ['error' => $exception->getMessage()];
    }

    return $workspaces_data;
  }

  private function getRequiredTableColumns() {
    $requiredProperties = [
      'Time',
      'Day',
      'Time Entry',
      'Project',
      'Task',
    ];
    return $requiredProperties;
  }

}
