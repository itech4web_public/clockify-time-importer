<?php

declare(strict_types=1);

namespace App\Repository;

class ClockifyTimeEntryCustomfields
{

    /**
     * Schema used to validate input for creating instances of this class
     *
     * @var array
     */
    private static array $schema = [
        'properties' => [
            'customFieldId' => [
                'type' => 'string',
            ],
            'value' => [
                'type' => 'string',
            ],
        ],
    ];

    /**
     * @var string|null
     */
    private ?string $customFieldId = null;

    /**
     * @var string|null
     */
    private ?string $value = null;

    /**
     *
     */
    public function __construct()
    {
    }

    /**
     * @return string|null
     */
    public function getCustomFieldId() : ?string
    {
        return isset($this->customFieldId) ? $this->customFieldId : null;
    }

    /**
     * @return string|null
     */
    public function getValue() : ?string
    {
        return isset($this->value) ? $this->value : null;
    }

    /**
     * @param string $customFieldId
     * @return self
     */
    public function withCustomFieldId(string $customFieldId) : self
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($customFieldId, static::$schema['properties']['customFieldId']);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException($validator->getErrors()[0]['message']);
        }

        $clone = clone $this;
        $clone->customFieldId = $customFieldId;

        return $clone;
    }

    /**
     * @return self
     */
    public function withoutCustomFieldId() : self
    {
        $clone = clone $this;
        unset($clone->customFieldId);

        return $clone;
    }

    /**
     * @param string $value
     * @return self
     */
    public function withValue(string $value) : self
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($value, static::$schema['properties']['value']);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException($validator->getErrors()[0]['message']);
        }

        $clone = clone $this;
        $clone->value = $value;

        return $clone;
    }

    /**
     * @return self
     */
    public function withoutValue() : self
    {
        $clone = clone $this;
        unset($clone->value);

        return $clone;
    }

    /**
     * Builds a new instance from an input array
     *
     * @param array $input Input data
     * @return ClockifyTimeEntryCustomfields Created instance
     * @throws \InvalidArgumentException
     */
    public static function buildFromInput(array $input) : ClockifyTimeEntryCustomfields
    {
        static::validateInput($input);

        $customFieldId = null;
        if (isset($input['customFieldId'])) {
            $customFieldId = $input['customFieldId'];
        }
        $value = null;
        if (isset($input['value'])) {
            $value = $input['value'];
        }

        $obj = new static();
        $obj->customFieldId = $customFieldId;
        $obj->value = $value;
        return $obj;
    }

    /**
     * Converts this object back to a simple array that can be JSON-serialized
     *
     * @return array Converted array
     */
    public function toJson() : array
    {
        $output = [];
        if (isset($this->customFieldId)) {
            $output['customFieldId'] = $this->customFieldId;
        }
        if (isset($this->value)) {
            $output['value'] = $this->value;
        }

        return $output;
    }

    /**
     * Validates an input array
     *
     * @param array $input Input data
     * @param bool $return Return instead of throwing errors
     * @return bool Validation result
     * @throws \InvalidArgumentException
     */
    public static function validateInput(array $input, bool $return = false) : bool
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($input, static::$schema);

        if (!$validator->isValid() && !$return) {
            $errors = array_map(function(array $e): string {
                return $e["property"] . ": " . $e["message"];
            }, $validator->getErrors());
            throw new \InvalidArgumentException(join(", ", $errors));
        }

        return $validator->isValid();
    }

    public function __clone()
    {
    }


}

