<?php

declare(strict_types=1);

namespace App\Repository;

class ClockifyTimeEntry
{

    /**
     * Schema used to validate input for creating instances of this class
     *
     * @var array
     */
    private static array $schema = [
        'required' => [
            'start',
            'description',
            'end',
            'projectId',
            'taskId',
        ],
        'properties' => [
            'start' => [
                'type' => 'string',
                'format' => 'date-time',
            ],
            'billable' => [
                'type' => 'boolean',
            ],
            'description' => [
                'type' => 'string',
            ],
            'projectId' => [
                'type' => 'string',
            ],
            'taskId' => [
                'type' => 'string',
            ],
            'end' => [
                'type' => 'string',
                'format' => 'date-time',
            ],
            'tagIds' => [
                'type' => 'array',
                'items' => [
                    'type' => 'string',
                ],
            ],
            'customfields' => [
                'properties' => [
                    'customFieldId' => [
                        'type' => 'string',
                    ],
                    'value' => [
                        'type' => 'string',
                    ],
                ],
            ],
        ],
    ];

    /**
     * @var \DateTime
     */
    private \DateTime $start;

    /**
     * @var bool|null
     */
    private ?bool $billable = null;

    /**
     * @var string
     */
    private string $description;

    /**
     * @var string
     */
    private string $projectId;

    /**
     * @var string
     */
    private string $taskId;

    /**
     * @var \DateTime
     */
    private \DateTime $end;

    /**
     * @var string[]|null
     */
    private ?array $tagIds = null;

    /**
     * @var ClockifyTimeEntryCustomfields|null
     */
    private ?ClockifyTimeEntryCustomfields $customfields = null;

    /**
     * @param \DateTime $start
     * @param string $description
     * @param string $projectId
     * @param string $taskId
     * @param \DateTime $end
     */
    public function __construct(\DateTime $start, string $description, string $projectId, string $taskId, \DateTime $end)
    {
        $this->start = $start;
        $this->description = $description;
        $this->projectId = $projectId;
        $this->taskId = $taskId;
        $this->end = $end;
    }

    /**
     * @return \DateTime
     */
    public function getStart() : \DateTime
    {
        return $this->start;
    }

    /**
     * @return bool|null
     */
    public function getBillable() : ?bool
    {
        return isset($this->billable) ? $this->billable : null;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getProjectId() : string
    {
        return $this->projectId;
    }

    /**
     * @return string
     */
    public function getTaskId() : string
    {
        return $this->taskId;
    }

    /**
     * @return \DateTime
     */
    public function getEnd() : \DateTime
    {
        return $this->end;
    }

    /**
     * @return string[]|null
     */
    public function getTagIds() : ?array
    {
        return isset($this->tagIds) ? $this->tagIds : null;
    }

    /**
     * @return ClockifyTimeEntryCustomfields|null
     */
    public function getCustomfields() : ?ClockifyTimeEntryCustomfields
    {
        return isset($this->customfields) ? $this->customfields : null;
    }

    /**
     * @param \DateTime $start
     * @return self
     */
    public function withStart(\DateTime $start) : self
    {
        $clone = clone $this;
        $clone->start = $start;

        return $clone;
    }

    /**
     * @param bool $billable
     * @return self
     */
    public function withBillable(bool $billable) : self
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($billable, static::$schema['properties']['billable']);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException($validator->getErrors()[0]['message']);
        }

        $clone = clone $this;
        $clone->billable = $billable;

        return $clone;
    }

    /**
     * @return self
     */
    public function withoutBillable() : self
    {
        $clone = clone $this;
        unset($clone->billable);

        return $clone;
    }

    /**
     * @param string $description
     * @return self
     */
    public function withDescription(string $description) : self
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($description, static::$schema['properties']['description']);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException($validator->getErrors()[0]['message']);
        }

        $clone = clone $this;
        $clone->description = $description;

        return $clone;
    }

    /**
     * @param string $projectId
     * @return self
     */
    public function withProjectId(string $projectId) : self
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($projectId, static::$schema['properties']['projectId']);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException($validator->getErrors()[0]['message']);
        }

        $clone = clone $this;
        $clone->projectId = $projectId;

        return $clone;
    }

    /**
     * @param string $taskId
     * @return self
     */
    public function withTaskId(string $taskId) : self
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($taskId, static::$schema['properties']['taskId']);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException($validator->getErrors()[0]['message']);
        }

        $clone = clone $this;
        $clone->taskId = $taskId;

        return $clone;
    }

    /**
     * @param \DateTime $end
     * @return self
     */
    public function withEnd(\DateTime $end) : self
    {
        $clone = clone $this;
        $clone->end = $end;

        return $clone;
    }

    /**
     * @param string[] $tagIds
     * @return self
     */
    public function withTagIds(array $tagIds) : self
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($tagIds, static::$schema['properties']['tagIds']);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException($validator->getErrors()[0]['message']);
        }

        $clone = clone $this;
        $clone->tagIds = $tagIds;

        return $clone;
    }

    /**
     * @return self
     */
    public function withoutTagIds() : self
    {
        $clone = clone $this;
        unset($clone->tagIds);

        return $clone;
    }

    /**
     * @param ClockifyTimeEntryCustomfields $customfields
     * @return self
     */
    public function withCustomfields(ClockifyTimeEntryCustomfields $customfields) : self
    {
        $clone = clone $this;
        $clone->customfields = $customfields;

        return $clone;
    }

    /**
     * @return self
     */
    public function withoutCustomfields() : self
    {
        $clone = clone $this;
        unset($clone->customfields);

        return $clone;
    }

    /**
     * Builds a new instance from an input array
     *
     * @param array $input Input data
     * @return ClockifyTimeEntry Created instance
     * @throws \InvalidArgumentException
     */
    public static function buildFromInput(array $input) : ClockifyTimeEntry
    {
        static::validateInput($input);

        $start = new \DateTime($input['start']);
        $billable = null;
        if (isset($input['billable'])) {
            $billable = (bool)($input['billable']);
        }
        $description = $input['description'];
        $projectId = $input['projectId'];
        $taskId = $input['taskId'];
        $end = new \DateTime($input['end']);
        $tagIds = null;
        if (isset($input['tagIds'])) {
            $tagIds = $input['tagIds'];
        }
        $customfields = null;
        if (isset($input['customfields'])) {
            $customfields = ClockifyTimeEntryCustomfields::buildFromInput($input['customfields']);
        }

        $obj = new static($start, $description, $projectId, $taskId, $end);
        $obj->billable = $billable;
        $obj->tagIds = $tagIds;
        $obj->customfields = $customfields;
        return $obj;
    }

    /**
     * Converts this object back to a simple array that can be JSON-serialized
     *
     * @return array Converted array
     */
    public function toJson() : array
    {
        $output = [];
        $output['start'] = ($this->start)->format(\DateTime::ATOM);
        if (isset($this->billable)) {
            $output['billable'] = $this->billable;
        }
        $output['description'] = $this->description;
        $output['projectId'] = $this->projectId;
        $output['taskId'] = $this->taskId;
        $output['end'] = ($this->end)->format(\DateTime::ATOM);
        if (isset($this->tagIds)) {
            $output['tagIds'] = $this->tagIds;
        }
        if (isset($this->customfields)) {
            $output['customfields'] = ($this->customfields)->toJson();
        }

        return $output;
    }

    /**
     * Validates an input array
     *
     * @param array $input Input data
     * @param bool $return Return instead of throwing errors
     * @return bool Validation result
     * @throws \InvalidArgumentException
     */
    public static function validateInput(array $input, bool $return = false) : bool
    {
        $validator = new \JsonSchema\Validator();
        $validator->validate($input, static::$schema);

        if (!$validator->isValid() && !$return) {
            $errors = array_map(function(array $e): string {
                return $e["property"] . ": " . $e["message"];
            }, $validator->getErrors());
            throw new \InvalidArgumentException(join(", ", $errors));
        }

        return $validator->isValid();
    }

    public function __clone()
    {
        $this->start = clone $this->start;
        $this->end = clone $this->end;
        if (isset($this->customfields)) {
            $this->customfields = clone $this->customfields;
        }
    }


}

