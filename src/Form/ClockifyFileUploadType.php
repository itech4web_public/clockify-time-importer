<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class ClockifyFileUploadType extends ClockifyTimeExporterType {

  public function buildForm(FormBuilderInterface $builder, array $options) {

    parent::buildForm($builder, $options);
    $builder->remove('save');

    $builder
      ->add('csv', FileType::class, [
        'label' => 'CSV',
        // unmapped means that this field is not associated to any entity property
        'mapped' => FALSE,
        'attr' => [
          'accept' => 'text/csv'
        ],
        'constraints' => [
          new File([
            'maxSize' => '1024k',
            'mimeTypes' => [
              'text/csv',
              'text/x-comma-separated-values',
              'text/x-csv',
              'text/plain'
            ],
            'mimeTypesMessage' => 'Please upload a valid CSV document',
          ]),
        ],
      ])
      ->add('save', SubmitType::class, ['label' => 'Upload & Validate']);

  }

}
