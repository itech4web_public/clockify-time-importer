<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ClockifyTimeExporterType extends AbstractType {

  private $apiClient;

  public function __construct(HttpClientInterface $clockifyHttpClient) {
    $this->apiClient = $clockifyHttpClient;
  }

  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->add('workspaces', ChoiceType::class, [
        'label' => 'Workspace',
        'choices' => $options['workspace_options'],
      ])
      ->add('save', SubmitType::class, ['label' => 'Import']);
  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults([
      'workspace_options' => $this->getWorkspacesOptions(),
      'default_workspace' => '',
    ]);

    // you can also define the allowed types, allowed values and
    // any other feature supported by the OptionsResolver component
    $resolver->setAllowedTypes('workspace_options', 'array');
    $resolver->setAllowedTypes('default_workspace', 'string');

  }

  private function getWorkspaces(): array {

    try {
      $response = $this->apiClient->request('GET', 'workspaces');
      $workspaces_data = $response->toArray();
    }
    catch (\Exception $exception) {
      return ['error' => $exception->getMessage()];
    }

    return $workspaces_data;
  }

  private function getWorkspacesOptions(): array {
    $workspaces_data = $this->getWorkspaces();
    $options = [];
    foreach ($workspaces_data as $workspaces_data_item) {
      $options[$workspaces_data_item['name']] = $workspaces_data_item['id'];
    }

    return $options;
  }

}
